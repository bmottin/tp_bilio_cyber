<?php

class KindController {
    public $kinds = array();
    private $db;

    public function __construct()
    {
        $this->db = new Database();
        $this->db->getConnection();
    }

    public function getAllKinds(){
        
        return $this->kinds;
        
    }

    public function getKindById($id){
        // TODO ajouter requete BDD
        foreach ($this->kinds as $key => $kind) {
            // je vais parcourir chaque genre fourni
            // si l'ID de mon instance de genre correspond à l'ID fourni
            // je retourne ce genre, 
            if($kind->getId() == $id) {
                return $kind;
            }                
        }
        // si après avoir parcouru la boucle
        // je ne touve pas de genre, je leve une Exception
        throw new Exception("Pas de genre correspondant", 1);
    }

}