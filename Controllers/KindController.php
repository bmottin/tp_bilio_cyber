<?php

class KindController extends ControllerDeBase  {
    
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllKinds(){
        // la requete pour récuprerer les enregistrements depuis la BDD
        $query = "SELECT id, name FROM kind";
        // je prepare la requete (optimisation du traitement + 
        // remplacement des valeurs par les variables fournis)
        $stmt = $this->db->conn->prepare($query);
        // j'execute la requete, mais le resultat n'est pas exploitable tel quel
        $stmt->execute();

        // je définit un tableau vide pour retourn mes enregistrements
        $res = array();

        // pour chaque enregistrement
        foreach ($stmt->fetchAll() as $key => $genre) {
            // j'instancie un nouvel objet de type Kind et 
            // fourni les arguments au contructeur
            $kind = new Kind($genre['id'], $genre['name']);
            // et j'ajoute cet objet à mon tableau de retour
            $res[] = $kind;
        }
        // je retourne mon tableau
        return $res;
        
    }

    public function getKindById($id){
        // TODO ajouter requete BDD
        // la requete pour récupérer les enregistrements depuis la BDD
        $query = "SELECT id, name FROM kind WHERE id = :id";
        
        // je prepare la requete (optimisation du traitement + 
        // remplacement des valeurs par les variables fournis)
        $stmt = $this->db->conn->prepare($query);
        // j'execute la requete en fournissant les valeurs à prendre en compte,
        // mais le resultat n'est pas exploitable tel quel
        $stmt->execute(array(':id' => $id));
        // un resultat sous forme de tableau associatif
        $res = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $kind = new Kind($res['id'], $res['name']);
        return $kind;

        // je ne touve pas de genre, je leve une Exception
        // throw new Exception("Pas de genre correspondant", 1);
    }

}