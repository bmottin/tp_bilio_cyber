<?php
class ControllerDeBase {
    // Si visibilité en private => l'attribue ne sera visible que depuis la classe en cours
    // Si visibilité en protected => l'attribue ne sera visible que depuis la classe et ses enfant
    // Si visibilité en public => l'attribue sera visible de partout
    protected $db;

    public function __construct()
    {
        $this->db = new Database();
        $this->db->getConnection();
    }

    
}