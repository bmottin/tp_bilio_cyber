<?php

class CountryController extends ControllerDeBase {

    public function getAllCountries(){
        // la requete pour récuprerer les enregistrements depuis la BDD
        $query = "SELECT id, iso_code, name FROM countries";
        // je prepare la requete (optimisation du traitement + 
        // remplacement des valeurs par les variables fournis)
        $stmt = $this->db->conn->prepare($query);
        // j'execute la requete, mais le resultat n'est pas exploitable tel quel
        $stmt->execute();

        // je définit un tableau vide pour retourn mes enregistrements
        $res = array();

        // pour chaque enregistrement
        foreach ($stmt->fetchAll() as $key => $pays) {
            // j'instancie un nouvel objet de type Kind et 
            // fourni les arguments au contructeur
            $country = new Country();
            $country->setId($pays['id']);
            $country->setIsoCode($pays['iso_code']);
            $country->setName($pays['name']);

            // et j'ajoute cet objet à mon tableau de retour
            $res[] = $country;
        }
        // je retourne mon tableau
        return $res;
        
    }

    public function getCountryById($id){
        // TODO ajouter requete BDD
        // la requete pour récupérer les enregistrements depuis la BDD
        $query = "
        SELECT 
            id, 
            iso_code, 
            name 
        FROM 
            countries 
        WHERE 
            id = :id";
        
        // je prepare la requete (optimisation du traitement + 
        // remplacement des valeurs par les variables fournis)
        $stmt = $this->db->conn->prepare($query);
        // j'execute la requete en fournissant les valeurs à prendre en compte,
        // mais le resultat n'est pas exploitable tel quel
        $stmt->execute(array(':id' => $id));
        // un resultat sous forme de tableau associatif
        $res = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $country = new Country($res['id'], $res['iso_code'], $res['name']);
        return $country;

        // je ne touve pas de genre, je leve une Exception
        // throw new Exception("Pas de genre correspondant", 1);
    }

}