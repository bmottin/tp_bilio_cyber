<?php
class AuthorController extends ControllerDeBase {

    public function getAllAuthors(){
        // la requete pour récuprerer les enregistrements depuis la BDD
        $query = "
        SELECT 
            id, 
            name, 
            forname  
        FROM 
            authors";

        // je prepare la requete (optimisation du traitement + 
        // remplacement des valeurs par les variables fournis)
        $stmt = $this->db->conn->prepare($query);
        // j'execute la requete, mais le resultat n'est pas exploitable tel quel
        $stmt->execute();

        // je définit un tableau vide pour retourn mes enregistrements
        $res = array();

        // pour chaque enregistrement
        foreach ($stmt->fetchAll() as $key => $auteur) {
            // j'instancie un nouvel objet de type Kind et 
            // fourni les arguments au contructeur
            $author = new Author();
            $author->setId($auteur['id']);
            $author->setName($auteur['name']);
            $author->setForname($auteur['forname']);

            // et j'ajoute cet objet à mon tableau de retour
            $res[] = $author;
        }
        // je retourne mon tableau
        return $res;
        
    }

    public function getAuthorById($id){
        
        // la requete pour récupérer les enregistrements depuis la BDD
        $query = "
        SELECT 
            authors.id, 
            authors.name, 
            forname as prenom,
            countries.id as country_id,
            countries.iso_code,
            countries.name as country_name
        FROM 
            authors
        INNER JOIN
            countries
        ON
            countries.id = authors.country_id
        WHERE 
            authors.id = :id";
        
        // je prepare la requete (optimisation du traitement + 
        // remplacement des valeurs par les variables fournis)
        $stmt = $this->db->conn->prepare($query);
        // j'execute la requete en fournissant les valeurs à prendre en compte,
        // mais le resultat n'est pas exploitable tel quel
        $stmt->execute(array(':id' => $id));
        // un resultat sous forme de tableau associatif
        $res = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $country = new Country($res['country_id'],$res['iso_code'],$res['country_name']);

        $author = new Author();
        $author->setId($res['id']);
        $author->setName($res['name']);
        $author->setForname($res['prenom']);
        $author->setCountry($country);

        return $author;

        // je ne touve pas de genre, je leve une Exception
        // throw new Exception("Pas de genre correspondant", 1);
    }

    public function getAuthorByIdBis($id){
        
        // la requete pour récupérer les enregistrements depuis la BDD
        $query = "
        SELECT 
            authors.id, 
            authors.name, 
            forname as prenom,
            country_id
        FROM 
            authors        
        WHERE 
            authors.id = :id";
        
        // je prepare la requete (optimisation du traitement + 
        // remplacement des valeurs par les variables fournis)
        $stmt = $this->db->conn->prepare($query);
        // j'execute la requete en fournissant les valeurs à prendre en compte,
        // mais le resultat n'est pas exploitable tel quel
        $stmt->execute(array(':id' => $id));
        // un resultat sous forme de tableau associatif
        $res = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $cc = new CountryController();
        $country = $cc->getCountryById($res['country_id']);

        $author = new Author();
        $author->setId($res['id']);
        $author->setName($res['name']);
        $author->setForname($res['prenom']);
        $author->setCountry($country);

        return $author;

        // je ne touve pas de genre, je leve une Exception
        // throw new Exception("Pas de genre correspondant", 1);
    }

}