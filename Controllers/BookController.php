<?php

class BookController
{
    public $books = array();

    public function __construct()
    {
    }

    public function getAllBooks()
    {
        return $this->books;
    }

    public function getBookById($id)
    {
        // TODO ajouter requete BDD
        foreach ($this->books as $key => $book) {
            // je vais parcourir chaque livre fourni
            // si l'ID de mon instance de livre correspond à l'ID fourni
            // je retourne ce livre, 
            if ($book->getId() == $id) {
                return $book;
            }
        }
        // si après avoir parcouru la boucle
        // je ne touve pas de livre, je leve une Exception
        throw new Exception("Pas de Livre correspondant", 10);
    }

    public function getBookByKind($kind)
    {
        $books = array();
        // TODO ajouter requete BDD
        foreach ($this->books as $key => $book) {
            
            // je vais parcourir chaque genre fourni
            // si l'instance de genre de mon instance de livre correspond à l'instance fourni
            // je retourne ce genre, 
            if ($book->getKind()->getId() == $kind->getId()) {
                $books[] = $book;
            }            
        }
        // si après avoir parcouru la boucle
        // je ne touve pas de genre, je leve une Exception
        if (count($books) > 0) {
            return $books;
        } else {
            throw new Exception("Pas de Livre correspondant", 10);
        }
    }
}
