<?php

class Database{

    private $server = "localhost";
    private $user = "root";
    private $password = "";
    private $dbname = "cyberapp_tp_biblio";
    private $port;

    public $conn;

    public function getConnection(){

        // reinitialise la connection pour éviter de saturer le serveur de BDD
        $this->conn = null;
        try {
            $this->conn = new PDO(
                "mysql:host=" . $this->server . ";port=" . $this->port . ";dbname=" . $this->dbname, $this->user, $this->password
            );
        } catch (Exception $e) {
            echo "ERREUR :" . $e->getMessage();
        } finally {
            echo "Je passe tout le temps dans le finally";
        }
        
        
    }
}