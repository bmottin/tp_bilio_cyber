<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fichier débug</title>
</head>
<body>
    
<?php

require_once "Tools/Database.php";

require_once "Models/Book.php";
require_once "Models/Kind.php";
require_once "Models/Country.php";
require_once "Models/Author.php";

require_once "Controllers/ControllerDeBase.php";
require_once "Controllers/KindController.php";
require_once "Controllers/BookController.php";
require_once "Controllers/CountryController.php";
require_once "Controllers/AuthorController.php";

$bookController = new BookController();
$kindController = new KindController();
$countryController = new CountryController();
$authorController = new AuthorController();

/* 
 * DEBUG
 * */
// je créé 3 genres pour tester le controller
/*$k1 = new Kind(1, "Horreur");
$k2 = new Kind(2, "fantastique");
$k3 = new Kind(3, "Romantique");
//*/
// je met ces 3 genres dans un tableau public pour simuler la requette à la BDD
/*array_push($kindController->kinds, $k1);
array_push($kindController->kinds, $k2);
$kindController->kinds[] = $k3;
//*/

// je créé 3 bouquins
/*$b1 = new Book(1, "LastMan T.1", "123456789", TRUE, "", null, $k1);
$b2 = new Book(2, "Le Seigneur des Moigneaux", "456789123", TRUE, "", null, $k2);
$b3 = new Book(3, "Le Silence des Anneaux", "789123456", TRUE, "", null, $k3);

$bookController->books[] = $b1;
$bookController->books[] = $b2;
$bookController->books[] = $b3;
//*/
/*
 * FIN Debug
 * */

//--------------------------------
// --- Genre/Kind ----------------
// -------------------------------
echo "<hr><h2>genres</h2>";
// On va recuperer tout les genre de livre (kinds)
$kinds = $kindController->getAllKinds();
echo "<ul>";
foreach ($kinds as $key => $kind) {
    echo "<li>" . $kind->getName() . "</li>";
}
echo "</ul>";

// recupérer un genre particulier
$kind = $kindController->getKindById(5);

echo "<h3>".$kind->getName()."</h3>";

//--------------------------------
// --- Pays ----------------------
// -------------------------------
echo "<hr><h2>Pays</h2>";
echo "<select>";

foreach ($countryController->getAllCountries() as $key => $country) {
    echo "<option name='country' value='". $country->getId()."'>
        [" . $country->getIsoCode() . "]
        " . $country->getName() . "
        </option>";
}
echo "</select>";


//--------------------------------
// --- Auteur --------------------
// -------------------------------
echo "<hr><h2>Auteurs</h2>";
echo "<ul>";
foreach ($authorController->getAllAuthors() as $key => $author) {
    echo "
        <li>
            <a href='show_author.php?id=" . $author->getId() . "'>
                " . $author->getName() . " " . $author->getForname() . "
            </a>
        </li>";
}
echo "</ul>";

//--------------------------------
// --- Livre ---------------------
// -------------------------------
/*
echo "<hr><h2>Livres</h2>";
$books = $bookController->getAllBooks();

echo "<ul>";
foreach ($books as $key => $book) {
    echo "<li>" . $book->getTitle() . "</li>";
}
echo "</ul>";

$book = $bookController->getBookById(2);

echo "
    <table border=1>
        <tr>
            <td>Id</td>
            <td>Titre</td>
            <td>Genre</td>
        </tr>";
echo "
        <tr>
            <td>" . $book->getId() . "</td>
            <td>" . $book->getTitle() . "</td>
            <td>" . $book->getKind()->getName() . "</td>
        </tr>
    </table>";

$books1 = $bookController->getBookByKind($k2);

foreach ($books1 as $key => $book) {
    echo $book->getTitle() . "<br>";
}

//*/