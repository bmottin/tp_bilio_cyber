<?php

require_once "Tools/Database.php";
require_once "Models/Author.php";
require_once "Models/Country.php";

require_once "Controllers/ControllerDeBase.php";
require_once "Controllers/AuthorController.php";
require_once "Controllers/CountryController.php";

$ac = new AuthorController();

if(isset($_GET['id'])){
    $author_id = $_GET['id'];
} else {
    $author_id = null;
}


$author = $ac->getAuthorById($author_id);
$author2 = $ac->getAuthorByIdBis($author_id);

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fiche auteur</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/cerulean/bootstrap.min.css">
</head>

<body>
    <?php 
        if($author_id) {
    ?>
    <div class="jumbotron">
        <h1 class="display-3">
            <?php echo $author->getForname() . " " . $author->getName() ?>
        </h1>
        <h2>
            <?php echo $author; ?>
        </h2>
        <p class="lead"><?php echo $author->getCountry()->getName(); ?></p>
    </div>
    <?php 
        } else {
            echo "<p style='color:red'> Pas d'auteur à afficher</p>";
        }
        ?>
    <hr>
    <div class="jumbotron">
        <h1 class="display-3">
            <?php echo $author2->getForname() . " " . $author2->getName() ?>
        </h1>
        <h2>
            <?php echo $author2; ?>
        </h2>
        <p class="lead"><?php echo $author2->getCountry()->getName(); ?></p>
    </div>
</body>

</html>