-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mer. 13 mai 2020 à 09:28
-- Version du serveur :  5.7.24
-- Version de PHP : 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cyberapp_tp_biblio`
--

-- --------------------------------------------------------

--
-- Structure de la table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `street1` varchar(255) COLLATE utf8_bin NOT NULL,
  `street2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `postal_code` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `address`
--

INSERT INTO `address` (`id`, `street1`, `street2`, `postal_code`, `city`, `user_id`) VALUES
(1, '1 rue du pou qui grimpe', NULL, '50200', 'Coutances', 1),
(2, '1 rue de la Vallée des rois', NULL, '14000', 'Caen', 2);

-- --------------------------------------------------------

--
-- Structure de la table `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `forname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `authors`
--

INSERT INTO `authors` (`id`, `name`, `forname`, `country_id`) VALUES
(1, 'Balack', NULL, 80),
(2, 'Sanlaville', 'Mickaël', 80),
(3, 'vives', NULL, 80),
(4, 'Koontz', 'Dean R.', 75),
(5, 'King', 'Stephen', 75);

-- --------------------------------------------------------

--
-- Structure de la table `author_book_rel`
--

CREATE TABLE `author_book_rel` (
  `book_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `author_book_rel`
--

INSERT INTO `author_book_rel` (`book_id`, `author_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 5),
(4, 4),
(5, 4);

-- --------------------------------------------------------

--
-- Structure de la table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `isbn` varchar(15) COLLATE utf8_bin NOT NULL,
  `is_available` tinyint(1) NOT NULL,
  `image_cov` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `editor_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_parution` datetime NOT NULL,
  `theme_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `books`
--

INSERT INTO `books` (`id`, `title`, `isbn`, `is_available`, `image_cov`, `editor_id`, `date_created`, `date_parution`, `theme_id`) VALUES
(1, 'Lastman Tome1', '234567891', 0, NULL, 1, '2020-04-21 12:45:09', '2020-04-13 12:45:09', 1),
(2, 'Lastman Tome 2', '123456789', 0, NULL, 1, '2020-04-28 12:45:09', '2020-04-28 12:45:09', 1),
(3, 'Ça', '45678913', 1, NULL, 2, '2020-03-09 15:22:42', '2020-04-21 15:22:42', 4),
(4, 'Les yeux foudroyés', '456789138', 1, NULL, 2, '2020-04-13 15:31:49', '2020-04-05 15:31:49', 3),
(5, 'Peur Bleue', '78979456413', 0, NULL, 3, '2020-04-20 15:31:49', '2020-04-06 15:31:49', 2);

-- --------------------------------------------------------

--
-- Structure de la table `borrows`
--

CREATE TABLE `borrows` (
  `id` int(11) NOT NULL,
  `date_return` datetime DEFAULT NULL,
  `date_burrow` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `borrows`
--

INSERT INTO `borrows` (`id`, `date_return`, `date_burrow`, `user_id`, `employee_id`, `book_id`) VALUES
(1, NULL, '2020-04-08 15:29:55', 1, 2, 2),
(2, NULL, '2020-04-08 15:29:55', 1, 2, 1),
(3, '2020-04-09 15:38:29', '2020-03-25 15:38:29', 1, 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `buildings`
--

CREATE TABLE `buildings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `contact` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `administrator` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `street1` varchar(255) COLLATE utf8_bin NOT NULL,
  `street2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `postal_code` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `buildings`
--

INSERT INTO `buildings` (`id`, `name`, `contact`, `administrator`, `street1`, `street2`, `postal_code`, `city`) VALUES
(1, 'Bibliothèque George Brassens\r\n', 'direction@bib-saint-lo.com', 'Mairie de saint Lô', '1 rue de la rue (et pk pas)', NULL, '50000', 'Saint Lô');

-- --------------------------------------------------------

--
-- Structure de la table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `iso_code` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `countries`
--

INSERT INTO `countries` (`id`, `iso_code`, `name`) VALUES
(1, 'is', 'name'),
(2, 'AF', 'Afghanistan'),
(3, 'ZA', 'Afrique du Sud'),
(4, 'AX', 'Åland, Îles'),
(5, 'AL', 'Albanie'),
(6, 'DZ', 'Algérie'),
(7, 'DE', 'Allemagne'),
(8, 'DD', 'Allemagne de l\'EST'),
(9, 'AD', 'Andorre'),
(10, 'AO', 'Angola'),
(11, 'AI', 'Anguilla'),
(12, 'AQ', 'Antarctique'),
(13, 'AG', 'Antigua et Barbuda'),
(14, 'AN', 'Antilles néerlandaises'),
(15, 'SA', 'Arabie Saoudite'),
(16, 'AR', 'Argentine'),
(17, 'AM', 'Arménie'),
(18, 'AW', 'Aruba'),
(19, 'AU', 'Australie'),
(20, 'AT', 'Autriche'),
(21, 'AZ', 'Azerbaïdjan'),
(22, 'BS', 'Bahamas'),
(23, 'BH', 'Bahrein'),
(24, 'BD', 'Bangladesh'),
(25, 'BB', 'Barbade'),
(26, 'BY', 'Bélarus'),
(27, 'BE', 'Belgique'),
(28, 'BZ', 'Bélize'),
(29, 'BJ', 'Bénin'),
(30, 'BM', 'Bermudes'),
(31, 'BT', 'Bhoutan'),
(32, 'BO', 'Bolivie (État plurinational de)'),
(33, 'BQ', 'Bonaire, Saint-Eustache et Saba'),
(34, 'BA', 'Bosnie-Herzégovine'),
(35, 'BW', 'Botswana'),
(36, 'BV', 'Bouvet, Ile'),
(37, 'BR', 'Brésil'),
(38, 'BN', 'Brunéi Darussalam'),
(39, 'BG', 'Bulgarie'),
(40, 'BF', 'Burkina Faso'),
(41, 'BI', 'Burundi'),
(42, 'CV', 'Cabo Verde'),
(43, 'KY', 'Caïmans, Iles'),
(44, 'KH', 'Cambodge'),
(45, 'CM', 'Cameroun'),
(46, 'CA', 'Canada'),
(47, 'CL', 'Chili'),
(48, 'CN', 'Chine'),
(49, 'CX', 'Christmas, île'),
(50, 'CY', 'Chypre'),
(51, 'CC', 'Cocos/Keeling (Îles)'),
(52, 'CO', 'Colombie'),
(53, 'KM', 'Comores'),
(54, 'CG', 'Congo'),
(55, 'CD', 'Congo, République démocratique du'),
(56, 'CK', 'Cook, Iles'),
(57, 'KR', 'Corée, République de'),
(58, 'KP', 'Corée, République populaire démocratique de'),
(59, 'CR', 'Costa Rica'),
(60, 'CI', 'Côte d\'Ivoire'),
(61, 'HR', 'Croatie'),
(62, 'CU', 'Cuba'),
(63, 'CW', 'Curaçao'),
(64, 'DK', 'Danemark'),
(65, 'DJ', 'Djibouti'),
(66, 'DO', 'Dominicaine, République'),
(67, 'DM', 'Dominique'),
(68, 'EG', 'Egypte'),
(69, 'SV', 'El Salvador'),
(70, 'AE', 'Emirats arabes unis'),
(71, 'EC', 'Equateur'),
(72, 'ER', 'Erythrée'),
(73, 'ES', 'Espagne'),
(74, 'EE', 'Estonie'),
(75, 'US', 'Etats-Unis d\'Amérique'),
(76, 'ET', 'Ethiopie'),
(77, 'FK', 'Falkland/Malouines (Îles)'),
(78, 'FO', 'Féroé, îles'),
(79, 'FJ', 'Fidji'),
(80, 'FI', 'Finlande'),
(81, 'FR', 'France'),
(82, 'GA', 'Gabon'),
(83, 'GM', 'Gambie'),
(84, 'GE', 'Géorgie'),
(85, 'GS', 'Géorgie du sud et les îles Sandwich du sud'),
(86, 'GH', 'Ghana'),
(87, 'GI', 'Gibraltar'),
(88, 'GR', 'Grèce'),
(89, 'GD', 'Grenade'),
(90, 'GL', 'Groenland'),
(91, 'GP', 'Guadeloupe'),
(92, 'GU', 'Guam'),
(93, 'GT', 'Guatemala'),
(94, 'GG', 'Guernesey'),
(95, 'GN', 'Guinée'),
(96, 'GW', 'Guinée-Bissau'),
(97, 'GQ', 'Guinée équatoriale'),
(98, 'GY', 'Guyana'),
(99, 'GF', 'Guyane française'),
(100, 'HT', 'Haïti'),
(101, 'HM', 'Heard, Ile et MacDonald, îles'),
(102, 'HN', 'Honduras'),
(103, 'HK', 'Hong Kong'),
(104, 'HU', 'Hongrie'),
(105, 'IM', 'Île de Man'),
(106, 'UM', 'Îles mineures éloignées des Etats-Unis'),
(107, 'VG', 'Îles vierges britanniques'),
(108, 'VI', 'Îles vierges des Etats-Unis'),
(109, 'IN', 'Inde'),
(110, 'IO', 'Indien (Territoire britannique de l\'océan)'),
(111, 'ID', 'Indonésie'),
(112, 'IR', 'Iran, République islamique d\''),
(113, 'IQ', 'Iraq'),
(114, 'IE', 'Irlande'),
(115, 'IS', 'Islande'),
(116, 'IL', 'Israël'),
(117, 'IT', 'Italie'),
(118, 'JM', 'Jamaïque'),
(119, 'JP', 'Japon'),
(120, 'JE', 'Jersey'),
(121, 'JO', 'Jordanie'),
(122, 'KZ', 'Kazakhstan'),
(123, 'KE', 'Kenya'),
(124, 'KG', 'Kirghizistan'),
(125, 'KI', 'Kiribati'),
(126, 'KW', 'Koweït'),
(127, 'LA', 'Lao, République démocratique populaire'),
(128, 'LS', 'Lesotho'),
(129, 'LV', 'Lettonie'),
(130, 'LB', 'Liban'),
(131, 'LR', 'Libéria'),
(132, 'LY', 'Libye'),
(133, 'LI', 'Liechtenstein'),
(134, 'LT', 'Lituanie'),
(135, 'LU', 'Luxembourg'),
(136, 'MO', 'Macao'),
(137, 'MK', 'Macédoine, l\'ex-République yougoslave de'),
(138, 'MG', 'Madagascar'),
(139, 'MY', 'Malaisie'),
(140, 'MW', 'Malawi'),
(141, 'MV', 'Maldives'),
(142, 'ML', 'Mali'),
(143, 'MT', 'Malte'),
(144, 'MP', 'Mariannes du nord, Iles'),
(145, 'MA', 'Maroc'),
(146, 'MH', 'Marshall, Iles'),
(147, 'MQ', 'Martinique'),
(148, 'MU', 'Maurice'),
(149, 'MR', 'Mauritanie'),
(150, 'YT', 'Mayotte'),
(151, 'MX', 'Mexique'),
(152, 'FM', 'Micronésie, Etats Fédérés de'),
(153, 'MD', 'Moldova, République de'),
(154, 'MC', 'Monaco'),
(155, 'MN', 'Mongolie'),
(156, 'ME', 'Monténégro'),
(157, 'MS', 'Montserrat'),
(158, 'MZ', 'Mozambique'),
(159, 'MM', 'Myanmar'),
(160, 'NA', 'Namibie'),
(161, 'NR', 'Nauru'),
(162, 'NP', 'Népal'),
(163, 'NI', 'Nicaragua'),
(164, 'NE', 'Niger'),
(165, 'NG', 'Nigéria'),
(166, 'NU', 'Niue'),
(167, 'NF', 'Norfolk, Ile'),
(168, 'NO', 'Norvège'),
(169, 'NC', 'Nouvelle-Calédonie'),
(170, 'NZ', 'Nouvelle-Zélande'),
(171, 'OM', 'Oman'),
(172, 'UG', 'Ouganda'),
(173, 'UZ', 'Ouzbékistan'),
(174, 'PK', 'Pakistan'),
(175, 'PW', 'Palaos'),
(176, 'PS', 'Palestine, Etat de'),
(177, 'PA', 'Panama'),
(178, 'PG', 'Papouasie-Nouvelle-Guinée'),
(179, 'PY', 'Paraguay'),
(180, 'NL', 'Pays-Bas'),
(181, 'XX', 'Pays inconnu'),
(182, 'ZZ', 'Pays multiples'),
(183, 'PE', 'Pérou'),
(184, 'PH', 'Philippines'),
(185, 'PN', 'Pitcairn'),
(186, 'PL', 'Pologne'),
(187, 'PF', 'Polynésie française'),
(188, 'PR', 'Porto Rico'),
(189, 'PT', 'Portugal'),
(190, 'QA', 'Qatar'),
(191, 'SY', 'République arabe syrienne'),
(192, 'CF', 'République centrafricaine'),
(193, 'RE', 'Réunion'),
(194, 'RO', 'Roumanie'),
(195, 'GB', 'Royaume-Uni de Grande-Bretagne et d\'Irlande du Nord'),
(196, 'RU', 'Russie, Fédération de'),
(197, 'RW', 'Rwanda'),
(198, 'EH', 'Sahara occidental'),
(199, 'BL', 'Saint-Barthélemy'),
(200, 'KN', 'Saint-Kitts-et-Nevis'),
(201, 'SM', 'Saint-Marin'),
(202, 'MF', 'Saint-Martin (partie française)'),
(203, 'SX', 'Saint-Martin (partie néerlandaise)'),
(204, 'PM', 'Saint-Pierre-et-Miquelon'),
(205, 'VA', 'Saint-Siège'),
(206, 'VC', 'Saint-Vincent-et-les-Grenadines'),
(207, 'SH', 'Sainte-Hélène, Ascension et Tristan da Cunha'),
(208, 'LC', 'Sainte-Lucie'),
(209, 'SB', 'Salomon, Iles'),
(210, 'WS', 'Samoa'),
(211, 'AS', 'Samoa américaines'),
(212, 'ST', 'Sao Tomé-et-Principe'),
(213, 'SN', 'Sénégal'),
(214, 'RS', 'Serbie'),
(215, 'SC', 'Seychelles'),
(216, 'SL', 'Sierra Leone'),
(217, 'SG', 'Singapour'),
(218, 'SK', 'Slovaquie'),
(219, 'SI', 'Slovénie'),
(220, 'SO', 'Somalie'),
(221, 'SD', 'Soudan'),
(222, 'SS', 'Soudan du Sud'),
(223, 'LK', 'Sri Lanka'),
(224, 'SE', 'Suède'),
(225, 'CH', 'Suisse'),
(226, 'SR', 'Suriname'),
(227, 'SJ', 'Svalbard et île Jan Mayen'),
(228, 'SZ', 'Swaziland'),
(229, 'TJ', 'Tadjikistan'),
(230, 'TW', 'Taïwan, Province de Chine'),
(231, 'TZ', 'Tanzanie, République unie de'),
(232, 'TD', 'Tchad'),
(233, 'CS', 'Tchécoslovaquie'),
(234, 'CZ', 'Tchèque, République'),
(235, 'TF', 'Terres australes françaises'),
(236, 'TH', 'Thaïlande'),
(237, 'TL', 'Timor-Leste'),
(238, 'TG', 'Togo'),
(239, 'TK', 'Tokelau'),
(240, 'TO', 'Tonga'),
(241, 'TT', 'Trinité-et-Tobago'),
(242, 'TN', 'Tunisie'),
(243, 'TM', 'Turkménistan'),
(244, 'TC', 'Turks-et-Caïcos (Îles)'),
(245, 'TR', 'Turquie'),
(246, 'TV', 'Tuvalu'),
(247, 'UA', 'Ukraine'),
(248, 'SU', 'URSS'),
(249, 'UY', 'Uruguay'),
(250, 'VU', 'Vanuatu'),
(251, '', 'Vatican : voir Saint-Siège'),
(252, 'VE', 'Venezuela (République bolivarienne du)'),
(253, 'VN', 'Viet Nam'),
(254, 'VD', 'Viet Nam (Sud)'),
(255, 'WF', 'Wallis et Futuna'),
(256, 'YE', 'Yémen'),
(257, 'YU', 'Yougoslavie'),
(258, 'ZR', 'Zaïre'),
(259, 'ZM', 'Zambie'),
(260, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Structure de la table `editors`
--

CREATE TABLE `editors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin,
  `image_logo` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `editors`
--

INSERT INTO `editors` (`id`, `name`, `description`, `image_logo`) VALUES
(1, 'Castorman', NULL, NULL),
(2, 'J\'ai lu', NULL, NULL),
(3, 'Glenat', NULL, NULL),
(4, 'Semic', NULL, NULL),
(5, 'Gallimar', NULL, NULL),
(6, 'upf', NULL, NULL),
(7, 'La presse de la Manche', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `building_id` int(11) NOT NULL,
  `position_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `employees`
--

INSERT INTO `employees` (`id`, `name`, `building_id`, `position_type_id`) VALUES
(1, 'John Snow', 1, 1),
(2, 'sam Gamégie', 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `kind`
--

CREATE TABLE `kind` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `kind`
--

INSERT INTO `kind` (`id`, `name`) VALUES
(1, 'Horreur'),
(2, 'Romance'),
(3, 'SF'),
(4, 'Fantasy'),
(5, 'Fantastique'),
(6, 'Historique'),
(7, 'Biographie'),
(8, 'Scientifique'),
(9, 'Manga'),
(10, 'BD Franco-Belge'),
(11, 'Comics');

-- --------------------------------------------------------

--
-- Structure de la table `kind_book_rel`
--

CREATE TABLE `kind_book_rel` (
  `book_id` int(11) NOT NULL,
  `kind_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `kind_book_rel`
--

INSERT INTO `kind_book_rel` (`book_id`, `kind_id`) VALUES
(3, 1),
(4, 1),
(5, 1),
(1, 9),
(2, 9);

-- --------------------------------------------------------

--
-- Structure de la table `position_type`
--

CREATE TABLE `position_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `position_type`
--

INSERT INTO `position_type` (`id`, `name`) VALUES
(1, 'Agent'),
(2, 'Bibliothécaire');

-- --------------------------------------------------------

--
-- Structure de la table `subscription_type`
--

CREATE TABLE `subscription_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `price` float NOT NULL,
  `description` text COLLATE utf8_bin,
  `duration` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `subscription_type`
--

INSERT INTO `subscription_type` (`id`, `name`, `price`, `description`, `duration`) VALUES
(1, 'basic', 10, 'Tarif de base, 3 livres pour 3 semaines', 21),
(2, 'basic + Media', 15, 'Comme pour basic mais avec 3 CD/DVD pour 3 semaines', 21);

-- --------------------------------------------------------

--
-- Structure de la table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `themes`
--

INSERT INTO `themes` (`id`, `name`) VALUES
(1, 'Combat'),
(2, 'monstre'),
(3, 'enfance'),
(4, 'clown');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `forname` varchar(255) COLLATE utf8_bin NOT NULL,
  `tel` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `initial_subscription` datetime NOT NULL,
  `date_subscription` datetime NOT NULL,
  `date_birth` date DEFAULT NULL,
  `subscription_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `forname`, `tel`, `mail`, `initial_subscription`, `date_subscription`, `date_birth`, `subscription_type_id`) VALUES
(1, 'Durand', 'Marc', '02.33.33.33.33', NULL, '2020-04-07 12:22:46', '2020-04-07 12:22:46', '1958-04-12', 1),
(2, 'Aldana', 'Richard', NULL, 'lastman@casterman.com', '2012-01-26 00:00:00', '2020-04-07 12:25:47', '1982-07-17', 2);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `author_book_rel`
--
ALTER TABLE `author_book_rel`
  ADD PRIMARY KEY (`book_id`,`author_id`);

--
-- Index pour la table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `borrows`
--
ALTER TABLE `borrows`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `buildings`
--
ALTER TABLE `buildings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `editors`
--
ALTER TABLE `editors`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `kind`
--
ALTER TABLE `kind`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `kind_book_rel`
--
ALTER TABLE `kind_book_rel`
  ADD PRIMARY KEY (`kind_id`,`book_id`);

--
-- Index pour la table `position_type`
--
ALTER TABLE `position_type`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `subscription_type`
--
ALTER TABLE `subscription_type`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `borrows`
--
ALTER TABLE `borrows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `buildings`
--
ALTER TABLE `buildings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;

--
-- AUTO_INCREMENT pour la table `editors`
--
ALTER TABLE `editors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `kind`
--
ALTER TABLE `kind`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `position_type`
--
ALTER TABLE `position_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `subscription_type`
--
ALTER TABLE `subscription_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
