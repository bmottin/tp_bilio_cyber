<?php

class Author {

    private $id;
    private $name;
    private $forname;
    private $country;

    public function __toString()
    {
        return $this->forname . " " . $this->name;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of forname
     */ 
    public function getForname()
    {
        return $this->forname;
    }

    /**
     * Set the value of forname
     *
     * @return  self
     */ 
    public function setForname($forname)
    {
        $this->forname = $forname;

        return $this;
    }

    /**
     * Get the value of country
     */ 
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set the value of country
     *
     * @return  self
     */ 
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}