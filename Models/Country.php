<?php

class Country {
    private $id;
    private $iso_code;
    private $name;

    public function __construct($id=null, $iso_code=null, $name="")
    {
        $this->id = $id;
        $this->iso_code = $iso_code;
        $this->name = $name;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of iso_code
     */ 
    public function getIsoCode()
    {
        return $this->iso_code;
    }

    /**
     * Set the value of iso_code
     *
     * @return  self
     */ 
    public function setIsoCode($iso_code)
    {
        $this->iso_code = $iso_code;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}