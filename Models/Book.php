<?php

class Book
{
    private $id;
    private $title;
    private $isbn;
    private $is_available;
    private $image_cov;
    private $editor;
    private $date_created;
    private $date_updated;
    private $kind;


    public function __construct(
        $id = null,
        $title = "",
        $isbn = "",
        $is_available = false,
        $image_cov = "",
        $editor = null,
        $kind = null
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->isbn = $isbn;
        $this->is_available = $is_available;
        $this->image_cov = $image_cov;
        $this->editor = $editor;
        $this->date_created = new DateTime();
        $this->date_updated = new DateTime();
        $this->kind = $kind;
    }

    /*
     * ACCESSEUR
     * */

    /**
     * Get the value of title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of isbn
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * Set the value of isbn
     *
     * @return  self
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * Get the value of image_cov
     */
    public function getImageCov()
    {
        return $this->image_cov;
    }

    /**
     * Set the value of image_cov
     *
     * @return  self
     */
    public function setImageCov($image_cov)
    {
        $this->image_cov = $image_cov;

        return $this;
    }

    /**
     * Get the value of editor
     */
    public function getEditor()
    {
        return $this->editor;
    }

    /**
     * Set the value of editor
     *
     * @return  self
     */
    public function setEditor($editor)
    {
        $this->editor = $editor;

        return $this;
    }

    /**
     * Get the value of date_created
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set the value of date_created
     *
     * @return  self
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * Get the value of date_updated
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set the value of dateUpdated
     *
     * @return  self
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;

        return $this;
    }

    /**
     * Get the value of kind
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Set the value of kind
     *
     * @return  self
     */
    public function setKind($kind)
    {
        $this->kind = $kind;

        return $this;
    }


    /**
     * Get the value of is_available
     */
    public function getIsAvailable()
    {
        return $this->is_available;
    }

    /**
     * Set the value of is_available
     *
     * @return  self
     */
    public function setIsAvailable($is_available)
    {
        $this->is_available = $is_available;

        return $this;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
